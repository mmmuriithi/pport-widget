<?php
namespace pPort\Widget;
class Manager
{

	public $widgets_build;
	public $active;
	public $config_path;
	public $methods_path;
	public $properties_path;

	public function __construct($config=[])
	{
		$this->config=$config;
	}

	public function load($wgt_name,$alias,$data_attributes=[])
	{
		$raw_wgt_name=$wgt_name;
		
		$this->active=ucfirst($wgt_name);
		$this->path='widgets'.DS.$wgt_name.DS;
		$this->config_path=$this->path.DS.'config'.DS;
		$this->methods_path=$this->config_path.'methods.php';
		$this->properties_path=$this->config_path.'properties.php';
		$this->attributes=$data_attributes;
		$wgt_path=$this->path.$wgt_name.'.php';	
		
			if(is_readable($wgt_path))
			{	
				require_once $wgt_path;
				$widget = new $wgt_name();
				$widget->type=$wgt_name;
				$widget->set_id($alias);

				$widget->registry=$this;
				$properties=file_exists($this->properties_path)?include($this->properties_path):[];
				$properties=array_merge($properties,$this->attributes);
				if($properties)
				{
					$widget->init_properties($properties);
				}
				return $widget;
			}
			else
			{
				throw new \Exception("Could not find the specified widget '".$wgt_name."'");
			}  	
	}

	public function register($widget)
	{
		$widgets_build=isset($this->widgets_build)?$this->widgets_build:new \stdClass();
		$widgets_build->{$widget->id}=$widget;
	}

	public static function json()
	{
		return json_encode(static::$widgets_build);
	}

	public static function get_active()
	{
		return static::$active;
	}
}

