<?php
namespace pPort\Widget;
class Builder
{
	public $script=FALSE;
	public $html='';
	public $inner_html='';
	public $id=FALSE;
	public $ui=FALSE;
	public $properties=array();
	public $view=false;
	public $html_attributes=FALSE;
	public $widgets=NULL;
	public $type=NULL;
	public $registry;
	

	private function _widget_folder()
	{
		$widget=get_class($this);
		foreach(array(ucfirst($widget),strtolower($widget))as $widget)
		{
			$wgt_fld_path='widgets'.DS.$widget.DS;	
			if(is_readable($wgt_fld_path))
			{
				return $wgt_fld_path;
			}
		}
		throw new Exception("Could not find the specified widget folder : '".$widget."'");
	}

	



	public function display($view_or_data=false,$data_or_return=null,$store=FALSE,$prep_assets=TRUE)
	{

		$this->before_render();


		if(!$view_or_data OR is_array($view_or_data))
		{
			$data=$view_or_data;
			$store=$data_or_return;
			//Get the view
			$wgt_view=($this->view)?$this->view:get_called_class();
			//Ui : Bootstrap,Jquery UI...

		}
		else
		{
			$wgt_view=$this->view=$view_or_data;
			$data=$data_or_return;

		}
		$wgt_view=($this->ui)?$this->ui."_".$wgt_view:$wgt_view;		
		
		//Get vars
		$properties=$this->properties;
		//Get properties set from the configuration and pass them to the widget view....
		$data=(is_array($data))?array_merge($properties,$data):$properties;	

		foreach(array(ucfirst($wgt_view),strtolower($wgt_view))as $wgt_view)
		{
			$wgt_v_path=$this->_widget_folder().'views'.DS.$wgt_view.'.php';	

			if(is_readable($wgt_v_path))
			{	

				if(is_array($data))
				{
					extract($data);
				}
				//Include utility

				ob_start();				
				include($wgt_v_path);

				$widget=ob_get_contents();
				
				ob_end_clean();
				
				$this->html=$widget;
				
				$this->registry->register($this);

				if($store==FALSE)
				{
					echo $widget;
					return;
				}
				else
				{
					return $widget;
				}
			}
		}		
		throw new \Exception("Could not find the specified widget view ".$wgt_view." for  : '".get_class($this)." widget'");		
	}


	

	public function pack($object,$holder='div')
	{
		$html=is_object($object)?$object->render(array(),TRUE):$object;
		$this->inner_html.="<".$holder.">".$html."</".$holder.">";
		return $this;
	}

	

	public function get_ui()
	{
		return $this->ui;
	}

	public function set_ui($ui)
	{
		$this->ui=$ui;
	}

	public function ui($ui=FALSE)
	{
		if($ui!==FALSE)
		{
			$this->set_ui($ui);
		}
		else
		{
			return $this->get_ui();
		}
	}

	public function init_properties(array $properties=array())
	{
		$this->properties=$properties;
		foreach($properties as $k=>$v)
		{
			$this->{$k}=$v;
		}
	}

	public function set_properties(array $properties=array())
	{
		$this->properties=array_merge($this->properties,$properties);
		return $this;
	}

	public function attach_property($key,$value)
	{
		$properties[$key]=$value;
		$this->properties=array_merge($this->properties,$properties);
		return $this;
	}

	public function allowed_properties()
	{
		//Get the widget config Config::widget('pButton');
		return $this->properties;
	}

	public function is_allowed_property($var)
	{
		return array_key_exists($var,$this->allowed_properties());
	}

	public function __get($var)
	{
		if(class_exists($var))
		{
			
		}
		elseif($this->is_allowed_property($var))
		{
			return $this->{$var};	
		}
		else
		{
			throw new \Exception("Undefined Property: $".get_called_class()."::".$var);
		}
	}

	public function __set($var,$value)
	{
		$this->{$var}=$value;
	}

	public function set_script($script)
	{
		$this->script=$script;
	}

	public function script($script=FALSE)
	{
		//Asset::register_script();
		if($args)
		{
			$this->set_script=$script;		
		}
		else
		{
			return $this->script;
		}				
	}


	public function attach($method,$closure)
	{		
		if(is_object($closure))
		{
			//Call the closure and get contents
			//Call the call-back definition passing the args and content
			$contents=call_user_func_array($closure,array());
			$definition=$this->define_method($contents);
		}	
		else
		{
			$definition=$closure;
		}
		$script=call_user_func_array(array($this,"attach_method"),array($method,$definition));
		Asset::register_script("mee",$script,"DOM_READY_HERE");
	}


	public function attach_method($method_name,$definition)
	{
		return $script='$("#'.$this->id.'")[0].'.$method_name.' ='.$definition.';';
		
	}

	public function trigger($method_name,$args=FALSE)
	{
		$script='$("#'.$this->id.'")[0].'.$method_name.'()';
		Asset::register_script("meegcgv",$script,"DOM_READY_HERE");
	}


	public function define_method($implementation)
	{
		return 'function(){
			'.$implementation.'
		}';
	}


	public function on($event,$listener=FALSE)
	{
		if(is_object($listener))
		{
			if(is_closure($listener))
			{
				$contents=call_user_func_array($listener,array());
				$call_back=$this->call_back_definition($contents);
			}
		}
		elseif(is_array($listener))
		{
			$obj=$listener[0];
			$event_called=$listener[1];
			$args=(isset($listener[2])&&is_object($listener[2]))?$listener[2]():FALSE;
			//$event_args=array_unshift($args,$event);

			//Check that config exists
			//vd(get_called_class());

			//vd($methods);
			$callback_definition=call_user_func_array(array($obj,"call"),array($event_called,$args));

			$call_back=$this->call_back_definition($callback_definition);


		}
		else
		{
			$script=$this->event($event,$call_back);
		}

		$script=$this->event($event,$call_back);
		Asset::register_script("event_".$event,$script);

	}



	public function call($event='click',$params=FALSE)
	{

		$methods=Config::widget(get_class($this),"methods");
		$method=arr($event,$methods);
		if($method)
		{
			return $method($this);
		}
		elseif(is_callable($this,'event_'.$event))
		{
			return call_user_func_array(array($this,'event_'.$event),$params);
		}

		return '$("#'.$this->id.'").'.$event.'();';
	}



	public function event($event='click',$call_back=FALSE,$type='bind')
	{
		//var_dump($call_back);
		if($call_back===FALSE&&$type=='call')
		{
			return '$("#'.$this->id.'").'.$event.'()';
		}
		else
		{
			return '$("#'.$this->id.'").bind("'.$event.'",'.$call_back.');';
		}
	}

	public function call_back_definition($implementation)
	{
		return 'function(){
			'.$implementation.'
		}';
	}



	public function add($object,$x="auto",$y="auto",$where="after")
	{
		$this->widgets=obj('widgets',$this,new stdClass());
		$this->widgets->{$object->id}=$object;
		$object->render(array(),TRUE,FALSE,FALSE);
		
		$temp_vars=array('content'=>$object->html,"left"=>$x,"top"=>$y);
		
		$obj_content=parse($this->child_holder(),$temp_vars);
		$this->inner_html=concat($this->inner_html,"\n".$obj_content,$where);
		return $this;
	}

	public function html()
	{
		return $this->render(array(),TRUE);
	}



	public function widgets()
	{
		return $this->widgets;
	}

	public function put($object,$x="auto",$y="auto",$where="after")
	{
		$obj_content=$object->show([],TRUE);
		$temp_vars=array('content'=>$obj_content,"left"=>$x,"top"=>$y);
		$obj_content=parse($this->child_holder(),$temp_vars);
		$this->inner_html=concat($this->inner_html,"\n".$obj_content,$where);
		return $this;
	}

	private function child_holder()
	{
		return "<div style='position:absolute;left:{left};top:{top};'>{content}</div>";
	}	

	public function append($object,$x=0,$y=0)
	{
		$this->add($object,$x,$y,"after");
	}	

	public function prepend($object,$x,$y)
	{
		$this->add($object,$x,$y,"before");
	}

	public function set_id($id)
	{
		$this->id=$id;
	}

	public function tag($tag,$value="",$attributes="")
	{
		$this->inner_html=$this->inner_html.Html::tag($tag,$value,$attributes);
		return $this;
	}

	public function __call($func,$args)
	{
		$func=explode('_',$func);
		$type=array_shift($func);
		$property=implode('_',$func);
		if($type=='set')
		{
			$this->$property=$args[0];
			return $this;
		}
		elseif($type=='get')
		{
			return $this->$property;
		}
	}





}

