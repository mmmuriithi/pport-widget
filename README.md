# README #
pPort Widget Manager Package : Easily enable creation of widgets.

Installation
============

Install pport\widget using composer : 

	composer require pport/widget

Initialize the Widget Manager
-----------
#### Step 1 : Creating a Sample Widget ####
Create a widgets folder, and add a sample widget implementation.

```php
<?php
	class Sample_widget extends \pPort\Widget\Builder
	{
		public function render($data=[])
		{
			//You can return content here
			//or use display function to render the widget views e.g. $this->display($data);
			echo "Sample widget content";
		}
	}
;?>
```
#### Step 2 : Loading & Using your widgets ####

```php
<?php
	$widget_manager=new pPort\Widget\Manager();	
	$sample_widget=$widget_manager->load('Sample_widget','sample');
	$sample_widget->render();
;?>
```